
#ifndef      PLAYER_H_
#define      PLAYER_H_

typedef  enum{false, true}   boolean;


void initialize(node * stack);
void push(node*top, int naip , int cod, int ind);
node* pop(node*top);
boolean empty( const node* stack);
boolean full(const stack * stack);


#endif   