#include <stdio.h>



int exemplo(void){


	struct horario      	// estrutura do tipo horario
	{
		int horas;
		int minutos;
		int segundos;
	}agora;       			//É a mesma coisa que fazer em separado em semelhança: int x; x = 17;

							// nesta linha, poderia-se  incializar a variavel tipo struct horario logo no inicio, fazendo, agora = {0, 12,20}; 
						

/* struct horario agora;    //seria a maneira de como  declararia-mos um variavel tipos estrutra horario. */  

							//A declaração de uma estrutura do tipo horario chamada "agora" já foi feita quando declaramos a estrutura  no inicio, logo, neste caso já não será necessário colocar o struct...

	agora.horas = 17;
	agora.minutos = 18;
						

	struct horario vect[5]; //exemplo de estrutura vector


	vect[0].hora = 10;
	vect[0].minuto = 20;


							//olhando para o vector da-nos a possibilidade de usar várias "pessoas"

	struct horario teste[3] = {{10,20,30},{10,20,40},{10,20,50}};


return 0;


}



 /*									*/
/*            Exemplo 2            */






/*

	struct telephone 		//exemplo de simples declaração de um estrutura
	{	
		char *name;
		int number;
	};


	int main()
	{
		struct telephone index;

		index.name = "Jane Doe";
		index.number = 12345;
		printf("Name: %s\n", index.name);
		printf("Telephone number: %d\n", index.number);

		return 0;
	}


	/* NOTA : Usando  o typedef, faz com que não seja necessario escrever o tipo da estrutura  e torna mais facil o uso de vários tipos de estruturas  */
 
	typedef struct telephone
	{
		char *name;
		int number;

	}TELEPHONE;

	int main()
	{
		TELEPHONE index;

		index.name = "Jane Doe";
		index.number = 12345;
		printf("Name: %s\n", index.name);
		printf("Telephone number: %d\n", index.number);

		return 0;
	


*/			