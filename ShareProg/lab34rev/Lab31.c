
#include <stdio.h>
#include <stdlib.h>

#define MAX_ROW 15
#define MAX_COL 15


/**************************************************************************
 							PROTO FUNCTIONS
 *************************************************************************/

void init_matrix(int row, int column, int m[][MAX_COL]);
void print_matrix(int row, int column, int matrix[][MAX_COL]);
void param(int *row, int *column);
void all_matrix(int row, int column,  int matrix1[][MAX_COL],int matrix2[][MAX_COL]);
void print(int row, int column, int matrix1[][MAX_COL],int matrix2[][MAX_COL],int matrix3[][MAX_COL]);
void mult_matrix(int row , int column ,int matrix1[][MAX_COL],int matrix2[][MAX_COL],int matrix3[][MAX_COL]);
void sum_matrix( int row , int column ,int matrix1[][MAX_COL],int matrix2[][MAX_COL]);



/**********************************************************************
 * Name:    init_matrix
 * Args:    row, column, m[][MAX_COL]
 * Return:  m
 * Desc:    Carrega matriz m
 **********************************************************************/



void init_matrix(int row, int column, int  m[][MAX_COL]){	
	
	int aux = 0;							//variavel que recebe o valor introduzido pelo utilizador

	
	printf("enter the matrix:\n");
	
	for(int i = 0; i < row; i++){
	    
	    for(int j = 0;j < column; j++){
	       
	        scanf("%d", &aux);

	        m[i][j] = aux;					//atribuicao do valor da variavel a matrix m
	    }
	
	printf("\n");

	}
		
		
}




/**********************************************************************
 * Name:    all_matrix
 * Args:    row, column, matrix1, matrix2
 * Return:   -
 * Desc:    Chama funcao init_matrix para as respectivas matrix1 e matrix2. 
 			Carregamento
 **********************************************************************/




void all_matrix(int row, int column,  int matrix1[][MAX_COL],int matrix2[][MAX_COL]){
	
	printf(" matrix 1\n");
	init_matrix( row,  column, matrix1);
	printf(" matrix 2\n");
	init_matrix( row,  column, matrix2);


}



/**********************************************************************
 * Name:    print_matrix
 * Args:    row , column, matrix
 * Return:  -
 * Desc:    imprime a matrix 
 **********************************************************************/



void print_matrix(int row, int column, int matrix[][MAX_COL]){

	
	for(int i = 0;i < row; i++){
	    
	    for(int j = 0;j < column; j++){
	    	
	        printf("%d \t",matrix[i][j]);}
	
	printf("\n");}

}




/**********************************************************************
 * Name:    print
 * Args:    row, column, matrix1 ,matrix2 ,matrix3
 * Return:  -
 * Desc:    imprime todas  as matrix's
 **********************************************************************/




void print(int row, int column, int matrix1[][MAX_COL],int matrix2[][MAX_COL],int matrix3[][MAX_COL]){

	printf("%i X %i\n \n",row, column );

	print_matrix( row,  column, matrix1);
	printf("\n");
	print_matrix( row,  column, matrix2);
	printf("\n");
	print_matrix( row,  column, matrix3);


}




/**********************************************************************
 * Name:    param
 * Args:    row , column
 * Return:  - 
 * Desc:    introducao dos parametros pelo introducao
 **********************************************************************/





void param(int *row, int *column){
	
	int m =0 , n = 0; 				// 

	printf("Introduza os seguintes parametros (Tip: nºlinhas = nºcolunas)\n");
	printf("number of rows :");
	scanf("%d",&n);

	printf("number of columns:");
	scanf("%d",&m);

	while( n != m){

		printf("#ERROR input - introduza novamente os parametros\n\n");

		printf("number of rows :");
		scanf("%d",&n);

		printf("number of columns:");
		scanf("%d",&m);



	}

	*row = n;  
	*column = m;


}




/**********************************************************************
 * Name:    sum_matrix
 * Args:    row, column, matrix1, matrix2
 * Return:  -
 * Desc:    faz a soma das matrizes 
 **********************************************************************/



void sum_matrix( int row , int column ,int matrix1[][MAX_COL],int matrix2[][MAX_COL]){


	for(int i = 0; i < row; i++){
	    
	    for(int j = 0;j < column; j++){
	    	
	        matrix1[i][j] =  matrix1[i][j] + matrix2[i][j];
	
		}
	}

}





/**************************************************************************
 * Name:    mult_matrix
 * Args:    c
 * Return:  row, column, matrix1, matrix2, matrix3
 * Desc:    multiplicacao das matrizes 1 e 2 e coloca o resultado na matriz 3
 *************************************************************************/




void mult_matrix(int row , int column ,int matrix1[][MAX_COL],int matrix2[][MAX_COL],int matrix3[][MAX_COL]){
		
		int sum = 0;
			
		for(int i = 0; i < row; i++){
	    
	    	for(int j = 0; j < column; j++){
	    	
	        	sum = 0;

	           for(int k = 0;k < column;k++)

	           sum = sum + (matrix1[i][k]) * (matrix2[k][j]);

	           matrix3[i][j]=sum;

       		
	
		}
	}

}


/**************************************************************************
 							MAIN FUNCTION
 *************************************************************************/


int main(void){


	char awns[3], str[3];
	int row = 0, column = 0, res = 0;									// nº linhas, nº colunas das matrizes 1 ,2 e 3  
	
	int matrix1[MAX_ROW][MAX_COL] = {{0}};          
	int matrix2[MAX_ROW][MAX_COL] = {{0}};
	int matrix3[MAX_ROW][MAX_COL] = {{0}};
	
	printf("Que Operação deseja?\n\n tecla m para fazer Multiplicacao,\n Ou tecla s para fazer a soma !\n\n");

	fgets(str,3,stdin);
    res = sscanf(str, "%s" , awns);

    if(res != 1){

    	printf("#ERROR input\n\nQue Operação deseja?\n\n");
    	fgets(str,3,stdin);
    	res = sscanf(str, "%s" , awns);

    }


    if (awns[0] == 's'){


    	param(&row ,&column);										// Chamada da funcao param para a introducao do parametros correspondentes ao nº de colunas e nº de linhas 
    	all_matrix( row,  column,   matrix1, matrix2); 				// Carregamento das matrizes 1 e 2 
    	print(row,column, matrix1,matrix2, matrix3);				// Imprime as respectivas matrizes 1 , 2 e 3 
    	sum_matrix(row ,column , matrix1 , matrix2);				// Soma das matrizes 1 e 2 colocando o resultado na matrix 1
    	print(row,column, matrix1,matrix2, matrix3);
    }

    else if ( awns[0] == 'm'){

    	param(&row ,&column);										// Chamada da funcao param para a introducao do parametros correspondentes ao nº de colunas e nº de linhas 
    	all_matrix( row,  column,   matrix1, matrix2); 				// Carregamento das matrizes 1 e 2 
    	print(row,column, matrix1,matrix2, matrix3);				// Imprime as respectivas matrizes 1 , 2 e 3 
    	mult_matrix(row ,column , matrix1 , matrix2, matrix3);   	// Multiplicacao das matrizes 1 e 2 , colocando o resultado na matriz 3
    	print(row,column, matrix1,matrix2, matrix3);

    }
	
	else { printf("#ERROR input - Por favor renicie o programa.\n"); }
	
	


	return 0;


}
