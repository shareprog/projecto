 
 #include <stdio.h>
 #include <stdlib.h>


/**********************************************************************
                    Proto Functions
 **********************************************************************/

long int convert_to_decimal(long int binary);
long int convert_to_bin(long int decimal);


/**********************************************************************
 * Name:    convert_to_decimal
 * Args:    binary
 * Return:  decimal
 * Desc:    converte um numero binario em decimal
 **********************************************************************/


long int convert_to_decimal(long int binary) {
   
   long int decimal = 0;
   int last,j = 1;


    while(binary!=0)
      {

      
         last= binary % 10; 

         if( last == 0 || last == 1){          // ultimo digito

              decimal = decimal+last*j;        //multiplica pela base 2 elevado a expoente correspondente a sua posicao  

              j=j*2;                           // base 2 
         
              binary = binary/10;  }           //retira o ultimo digito
    
         else{                                 //verifica se esta na base 2
                
                printf("introduziu o numero em base diferente de 2\n");
                exit (0);                      

              }
      }


    return decimal;
}



/**********************************************************************
 * Name:    convert_to_bin
 * Args:    decimal
 * Return:  bin
 * Desc:    Converte um numero em decimal para binario
 **********************************************************************/



long int convert_to_bin(long int decimal){

        int i=0;
        long int bin = 0;
        int r = 1;
      
      while(decimal>0) 
      {     
           bin = (bin) + (decimal % 2)* r;   //resto da divisao por 2, colocado de forma a ler o numero ao contrario
           decimal = decimal / 2;           
           r *=10 ;
      }
    
    return bin;
}

/**********************************************************************
                    Main Fucntion
 **********************************************************************/

int main(void)
{


long int number = 0;
long int bin , decimal, o;

printf("introduza a operacao que deseja com b (binary) ou d (decimal)\n");

o = getchar();                                    // variavel que fica com o indicativo da operacao de conversao

  if (o == 'b' || o == 'd')                       // testa os parametro de entrada, caso esteja algo errado sai logo do programa
    {
      printf("introduza um número\n");
      
      scanf("%ld", &number);
      
      

      if( o == 'b' ){
      
            bin = convert_to_bin( number);

            printf("O resultado da convercao: %ld\n",bin);}
      
     else { decimal =convert_to_decimal(number);

            printf("O resultado da convercao: %ld\n",decimal);}

           }


  else 
    {
      printf("introduziu mal o indicativo\n"); 
      exit(0);
    }





return 0;

}