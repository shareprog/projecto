#include <stdlib.h>
#include <stdio.h>


#define MAXCARDS 13 	



typedef struct cartas {

	int naipe;
	int tipo;
}carta;


void ler(FILE *f1, carta * mao){

	int i = 0;
	int card ,type;
	char line[MAXCARDS];				//usado para receber do fgets

	while(fgets(line, sizeof(line), f1)!=NULL) {  //pego na linha e coloco numa string

		sscanf(line, "%d %d",&type , &card);		//mudo para inteiro
		
		//printf("teste type %d e card = %d\n",type,card);		//teste
		

		mao[i].naipe = card; // cena vinda do file.txt
		mao[i].tipo = type;

		//printf("naipe = %d e carta = %d\n",mao[i].naipe ,mao[i].tipo );
		++i;

	}

}

int ace(carta * mao ){


	int nace, i;

	nace = 0;

	for(i=0 ; i<MAXCARDS; i++){

		//printf("mao %d i = %d \n",mao[i].naipe, i);

		if(mao[i].naipe == 13){

			//printf("nºace = %d\n",nace);
			++nace; 
		}

	}


	return nace;
}

int main(void){

	
	int i; 

	FILE *f1;
	carta * hand = NULL;
	 	
	f1 = fopen("card2.txt", "r");
	hand = (carta*)malloc(sizeof(carta) * MAXCARDS);


   if(f1 == NULL)
   {
      printf("File 1 Pointer is invalid\n");
      return -1;
   }

   ler(f1,hand);
   ace(hand);

   printf("nº ace %d\n",ace(hand));

   free(hand);

return 0;

}