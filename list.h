
#ifndef      LIST_H_
#define      LIST_H_

#define MAXNOME 200


/*
Estrutura para respresentacao das cartas 
*/
typedef struct cartas 
{
    
    int naipe;
    int cod;

}carta;


/*
Lista para as cartas
*/

typedef struct baralho
{
    carta val;
    int indice;
    struct baralho *next; 

}nocartas;

/*
 Stack
*/

typedef struct stack {
   int cnt; 
   nocartas *top;

} node;
 

/*
Estrutura para respresentacao das jogadores
*/
typedef struct jogadores{

    char nome[MAXNOME];

    float dinheiro;
    float aposta;
    
    int ind;
    int ganhou, empate, perdeu;
    int enable;
    int ea;
    int bj, stand, hit, ndouble;

    node *pilha;

}jogador;


/*
Lista para os jogadores
*/


typedef struct slots{

    jogador playload;
    struct slots *next;

}nojogador;



nocartas *inserthead( nocartas * head, int nelements);
void printlista (nocartas * list);
nocartas *pickRandom(nocartas *head);
void deleteNode(nocartas *head, nocartas *n);


#endif          