#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include "list.h"


/**********************************************************************
 * Name:    inserthead
 * Args:    head, nelements
 * Return:  ponteiro do primeiro elemento da lista
 * Desc:    Adiciona elementos no incio da lista
 **********************************************************************/

nocartas *inserthead( nocartas * head, int nelements){

    int i ,z , t;
    int n = 3 , c = 12,ind = 0;

    nocartas * current;


    for (t = 0; t < nelements; ++t)
    { 

      ++ind; 
     
          for(i=0;i <= n;i++) {
            
              for(z =0; z <= c; z++) {
            
                current = (nocartas*)malloc(sizeof(nocartas));
                current->val.naipe = i;
                current->val.cod = z;
                current->indice = ind; 
                current->next  = head;
                head = current;


                }

          }

    }


    return current;


}


/**********************************************************************
 * Name:    printlista
 * Args:    head_list
 * Return:  
 * Desc:    imprime a lista
 **********************************************************************/



void printlista (nocartas * list){


   while(list!=NULL) {
      printf(" indice %i naipe:%i carta:%i \n", list->indice, list->val.naipe, list->val.cod);
      list = list->next;
   }


}



/**********************************************************************
 * Name:    printRandom
 * Args:    head_list
 * Return:  
 * Desc:   random node from a linked list
 **********************************************************************/



nocartas *pickRandom(nocartas *head)
{

  int n;
  int naipe , code, ind;
    
    if (head == NULL)
       return 0;
 
    
    srand(time(NULL));
 
    // Initialize result as first node
    
 
    // Iterate from the (k+1)th element to nth element
    nocartas *current = head;
    nocartas * temp;
    

    for (n=1; current!=NULL; n++)
    {
        // change result with probability 1/n
        if (rand() % n == 0){
           naipe = current->val.naipe;
           code = current->val.cod;
           ind = current->indice;
           temp = current;
         }
        // Move to next node
        current = current->next;
    }
 
    printf("Randomly selected node is n:%d c:%d i: %i \n", naipe, code, ind);



    return temp;
}





/**********************************************************************
 * Name:    
 * Args:    head_list
 * Return:  
 * Desc:   Elimina no de uma lista
 **********************************************************************/


void deleteNode(nocartas *head, nocartas *n)
{
    // When node to be deleted is head node
    if(head == n)
    {
        if(head->next == NULL)
        {
            printf("There is only one node. The list can't be made empty ");
            return ;
        }
 
        /* Copy the data of next node to head  */
        head->val.cod = head->next->val.cod;
        head->val.naipe = head->next->val.naipe;
        head->indice = head->next->indice;

        // store address of next node
        n = head->next;
 
        // Remove the link of next node
        head->next = head->next->next;
 
        // free memory
        free(n);
 
        return ;
    }
 
 
    // When not first node, follow the normal deletion process
 
    // find the previous node
    nocartas *prev = head;
    while(prev->next != NULL && prev->next != n)
        prev = prev->next;
 
    // Check if node really exists in Linked List
    if(prev->next == NULL)
    {
        printf("\n Given node is not present in Linked List");
        return;
    }
 
    // Remove node from Linked List
    prev->next = prev->next->next;
 
    // Free memory
    free(n);
 
    return; 
}







/*

void push(nocartas * head) {
    
  int i;

    nocartas * current = head;
    
    while (current->next != NULL) {
        current = current->next;
    }

   
    
    current->next = malloc(sizeof(nocartas));
    current->next->val.naipe = 9;
    current->next->next = NULL;
  

}

*/