
#include <stdio.h>

/*Max number of characters to be read/write from file*/
#define MAX_CHAR_FOR_FILE_OPERATION 1000000 

int read_and_show_the_file(char *filename)
{  
   FILE *fp;
   char text[MAX_CHAR_FOR_FILE_OPERATION];
   int i;

   fp = fopen(filename, "r");

   if(fp == NULL)
   {
      printf("File Pointer is invalid\n");
      return -1;
   }
   //Ensure array write starts from beginning
   i = 0;

   //Read over file contents until either EOF is reached or maximum characters is read and store in character array
   while( (fgets(&text[i++],sizeof(char)+1,fp) != NULL) && (i<MAX_CHAR_FOR_FILE_OPERATION) ) ;

   //Ensure array read starts from beginning
   i = 0;

   while((text[i] != '\0') && (i<MAX_CHAR_FOR_FILE_OPERATION) )
   {
      printf("%c",text[i++]);
   }

   fclose(fp);

   return 0;
}

int main(int argc, char *argv[])
{
   if(argc != 2)
   {
      printf("Execute the program along with file name to be read and printed. \n\
              \rFormat : \"%s <file-name>\"\n",argv[0]);
      return -1;
   }

   char *filename = argv[1];

   if( (read_and_show_the_file(filename)) == 0)
   {
      printf("File Read and Print to stdout is successful\n");
   }
   return 0;
}  



#include <stdio.h>
#include <stdlib.h>

FILE *f = fopen("textfile.txt", "rb");
fseek(f, 0, SEEK_END);
long fsize = ftell(f);
fseek(f, 0, SEEK_SET);  //same as rewind(f);

char *string = malloc(fsize + 1);
fread(string, fsize, 1, f);
fclose(f);

string[fsize] = 0;


#include <stdio.h>      /* For printf and file management */
#include <stdlib.h>     /* For dynamic memory allocation */
#include <string.h>     /* For string functions */

/* 
 * Read all lines from text file, and store them in a dynamically 
 * allocated array. 
 * The count of lines is stored in the 'count' output parameter
 * The caller is responsible for freeing the allocated memory.
 */
char** read_lines(FILE* txt, int* count) {
    char** array = NULL;        /* Array of lines */
    int    i;                   /* Loop counter */
    char   line[100];           /* Buffer to read each line */
    int    line_count;          /* Total number of lines */
    int    line_length;         /* Length of a single line */

    /* Clear output parameter. */
    *count = 0;

    /* Get the count of lines in the file */
    line_count = 0;
    while (fgets(line, sizeof(line), txt) != NULL) {                                      
       line_count++;
    }

    /* Move to the beginning of file. */
    rewind(txt);

    /* Allocate an array of pointers to strings 
     * (one item per line). */
    array = malloc(line_count * sizeof(char *));
    if (array == NULL) {
        return NULL; /* Error */
    }

    /* Read each line from file and deep-copy in the array. */
    for (i = 0; i < line_count; i++) {    
        /* Read the current line. */
        fgets(line, sizeof(line), txt);

        /* Remove the ending '\n' from the read line. */
        line_length = strlen(line);        
        line[line_length - 1] = '\0';
        line_length--; /* update line length */

        /* Allocate space to store a copy of the line. +1 for NUL terminator */
        array[i] = malloc(line_length + 1);

        /* Copy the line into the newly allocated space. */
        strcpy(array[i], line);
    }

    /* Write output param */
    *count = line_count;

    /* Return the array of lines */
    return array;
}

int main(int argc, char * argv[]) {
    char**      array    = NULL;    /* Array of read lines */
    FILE*       file     = NULL;    /* File to read lines from */
    const char* filename = NULL;    /* Name of the input file */
    int         i;                  /* Loop index */
    int         line_count;         /* Total number of read lines *7

    /* Get filename from the command line. */
    if (argc != 2) {
        printf("Specify the input file from the command line.\n");
        return 1;
    }    
    filename = argv[1];

    /* Try opening the file. */
    file = fopen(filename, "rt");
    if (file == NULL) {
        printf("Can't open file %s.\n", filename);
        return 1;
    }

    /* Read lines from file. */
    array = read_lines(file, &line_count);

    /* Just for test, print out the read lines. */
    for (i = 0; i < line_count; i++) {
        printf("[%d]: %s\n", (i+1), array[i]);
    }

    /* Cleanup. */
    fclose(file);
    for (i = 0; i < line_count; i++) {
        free(array[i]);
    }
    free(array);
    array = NULL;

    /* All right */
    return 0;
}


int main() {
    const char *f = "First";
    const char *s = "Second";
    char *tmp = malloc(strlen(f) + strlen(s) + 1);
    strcpy(tmp, f);
    strcpy(tmp+strlen(f), s);
    printf("%s", tmp);
    free(tmp);
    return 0;
}

#define MAX 1024

int main() {
    const char *f = "First";
    const char *s = "Second";
    size_t len_f = min(strlen(f), MAX);
    size_t len_s = min(strlen(s), MAX);
    size_t len_total = len_f + len_s;
    char *tmp = malloc(len_total + 1);
    strncpy(tmp, f, len_f);
    strncpy(tmp+len_f, s, len_s);
    tmp[len_total] = '\0';
    printf("%s", tmp);
    free(tmp);
    return 0;
}